package com.fzb.http.server.impl;

import com.fzb.http.server.HttpRequest;
import com.fzb.http.server.HttpRequestMethod;
import com.fzb.http.server.HttpResponse;

public class HttpServer implements HttpRequestMethod{

	@Override
	public void doGet(HttpRequest request, HttpResponse response) {
		
	}

	@Override
	public void doPost(HttpRequest request, HttpResponse response) {
		
	}

	@Override
	public void doPut(HttpRequest request, HttpResponse response) {
		
	}

	
}
